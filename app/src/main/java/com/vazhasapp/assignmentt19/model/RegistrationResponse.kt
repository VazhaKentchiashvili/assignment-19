package com.vazhasapp.assignmentt19.model

data class RegistrationResponse(
    val idToken: String?,
    val email: String?,
    val refreshToken: String?,
    val expiresIn: String?,
    val localId: String?,
)
