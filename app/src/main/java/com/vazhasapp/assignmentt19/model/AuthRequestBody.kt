package com.vazhasapp.assignmentt19.model

data class AuthRequestBody(
    val email: String?,
    val password: String?,
    val returnSecureToken: Boolean? = true,
)
