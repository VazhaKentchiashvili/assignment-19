package com.vazhasapp.assignmentt19.extensions

import android.widget.EditText

fun EditText.clearTexts() {
    text.clear()
}