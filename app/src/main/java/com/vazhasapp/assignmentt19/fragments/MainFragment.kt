package com.vazhasapp.assignmentt19.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vazhasapp.assignmentt19.R
import com.vazhasapp.assignmentt19.databinding.FragmentMainBinding
import com.vazhasapp.assignmentt19.model.AuthRequestBody
import com.vazhasapp.assignmentt19.viewmodels.LoginViewModel

class MainFragment : BaseFragment<FragmentMainBinding, LoginViewModel>(
    FragmentMainBinding::inflate,
    LoginViewModel::class.java,
) {

    override fun initialized(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnSignup.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_registrationFragment)
        }

        binding.btnLogin.setOnClickListener {
            mViewModel.user = getEnteredUser()
            mViewModel.init()
        }
    }

    private fun getEnteredUser(): AuthRequestBody {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        val token = true

        return AuthRequestBody(email, password, token)
    }
}