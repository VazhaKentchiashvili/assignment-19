package com.vazhasapp.assignmentt19.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.vazhasapp.assignmentt19.R
import com.vazhasapp.assignmentt19.viewmodels.RegistrationViewModel

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding, VM: ViewModel>(
    private val inflate: Inflate<VB>,
    myViewModel: Class<VM>
) : Fragment() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    protected val mViewModel: VM by lazy {
        ViewModelProvider(this).get(myViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate.invoke(inflater, container, false)
        }
        initialized(inflater, container)
        return binding.root
    }

    abstract fun initialized(inflater: LayoutInflater, container: ViewGroup?)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}