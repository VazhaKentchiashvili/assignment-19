package com.vazhasapp.assignmentt19.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vazhasapp.assignmentt19.databinding.FragmentRegistrationBinding
import com.vazhasapp.assignmentt19.extensions.clearTexts
import com.vazhasapp.assignmentt19.model.AuthRequestBody
import com.vazhasapp.assignmentt19.viewmodels.RegistrationViewModel

class RegistrationFragment : BaseFragment<FragmentRegistrationBinding, RegistrationViewModel>(
    FragmentRegistrationBinding::inflate,
    RegistrationViewModel::class.java,
) {

    override fun initialized(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnRegister.setOnClickListener {
            mViewModel.init()
            mViewModel.user = getEnteredUser()
            getEnteredUser()
            binding.etRegisterEmail.clearTexts()
            binding.etRegisterPassword.clearTexts()
            findNavController().popBackStack()
        }
    }

    private fun getEnteredUser(): AuthRequestBody {
        val email = binding.etRegisterEmail.text.toString()
        val password = binding.etRegisterPassword.text.toString()
        val token = true

        return AuthRequestBody(email, password, token)
    }
}