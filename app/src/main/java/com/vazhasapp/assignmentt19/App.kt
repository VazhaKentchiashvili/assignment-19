package com.vazhasapp.assignmentt19

import android.app.Application

class App: Application() {

    private external fun apiKey(): String

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
        lateinit var apiKey: String
    }

    override fun onCreate() {
        super.onCreate()
        apiKey = apiKey()
    }
}