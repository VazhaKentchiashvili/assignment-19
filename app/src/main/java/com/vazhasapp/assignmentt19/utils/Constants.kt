package com.vazhasapp.assignmentt19.utils

object Constants {
    const val BASE_URL = "https://identitytoolkit.googleapis.com"
    const val REGISTRATION_API_ENDPOINT = "/v1/accounts:signUp"
    const val LOGIN_API_ENDPOINT = "/v1/accounts:signInWithPassword"
}