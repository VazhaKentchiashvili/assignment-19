package com.vazhasapp.assignmentt19.viewmodels

import android.util.Log.d
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.assignmentt19.App
import com.vazhasapp.assignmentt19.api.RegistrationApiService
import com.vazhasapp.assignmentt19.fragments.BaseFragment
import com.vazhasapp.assignmentt19.fragments.MainFragment
import com.vazhasapp.assignmentt19.model.AuthRequestBody
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel : ViewModel() {

    lateinit var user: AuthRequestBody

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    fun init() {
        personLogin()
    }

    private fun personLogin() {
        viewModelScope.launch {
            withContext(ioDispatcher) {
               RegistrationApiService.registerPerson.loginPerson(App.apiKey, user)
            }
        }
    }
}