package com.vazhasapp.assignmentt19.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.assignmentt19.App
import com.vazhasapp.assignmentt19.api.RegistrationApiService
import com.vazhasapp.assignmentt19.model.AuthRequestBody
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegistrationViewModel : ViewModel() {

    lateinit var user: AuthRequestBody
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO


    fun init() {
        registerPerson()
    }

    private fun registerPerson() {
         viewModelScope.launch {
             withContext(ioDispatcher) {
                 RegistrationApiService.registerPerson.registerPerson(App.apiKey, user)
             }
         }
    }
}