package com.vazhasapp.assignmentt19.api

import com.vazhasapp.assignmentt19.utils.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RegistrationApiService {
    private val retrofitBuilder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val registerPerson by lazy {
        retrofitBuilder.create(RegisterRepository::class.java)
    }
}