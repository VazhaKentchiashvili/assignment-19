package com.vazhasapp.assignmentt19.api

import com.vazhasapp.assignmentt19.App
import com.vazhasapp.assignmentt19.model.RegistrationResponse
import com.vazhasapp.assignmentt19.model.AuthRequestBody
import com.vazhasapp.assignmentt19.model.LoginResponse
import com.vazhasapp.assignmentt19.utils.Constants.LOGIN_API_ENDPOINT
import com.vazhasapp.assignmentt19.utils.Constants.REGISTRATION_API_ENDPOINT
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface RegisterRepository {

    @POST(REGISTRATION_API_ENDPOINT)
    suspend fun registerPerson(
        @Query("key")
        apiKey: String = App.apiKey,

        @Body person: AuthRequestBody
    ): Response<RegistrationResponse>

    @POST(LOGIN_API_ENDPOINT)
    suspend fun loginPerson(
        @Query("key")
        apiKey: String = App.apiKey,

        @Body
        person: AuthRequestBody
    ): Response<LoginResponse>

}